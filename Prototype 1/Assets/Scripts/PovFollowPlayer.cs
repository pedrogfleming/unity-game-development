using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PovFollowPlayer : MonoBehaviour
{
    public GameObject player;
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        this.offset = new Vector3(0, 4, 2);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        this.transform.position = player.transform.position+ this.offset;
    }
}
