using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //public List <Camera> cameras;
    //private int currentCameraIndex;
    public Camera mainCamera;
    public Camera povCamera;
    // Use this for initialization
    void Start()
    {
        #region Viejo
        //currentCameraIndex = 0;
        ////Camera[] arrayCams = gameObject.GetComponents<Camera>();
        ////var cams = gameObject.GetComponentsInChildren<Camera>();        
        //cameras = new List<Camera>(gameObject.GetComponentsInChildren<Camera>());
        ////Turn all cameras off, except the first default one
        //for (int i = 1; i < cameras.Count; i++)
        //{
        //    cameras[i].gameObject.SetActive(false);
        //}

        ////If any cameras were added to the controller, enable the first one
        //if (cameras.Count > 0)
        //{
        //    cameras[0].gameObject.SetActive(true);
        //    //Debug.Log("Camera with name: " + cameras[0].camera.name + ", is now enabled");
        //}
        #endregion
        mainCamera.enabled = true;
        povCamera.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //If the v button is pressed, switch to the next camera
        //Set the camera at the current index to inactive, and set the next one in the array to active
        //When we reach the end of the camera array, move back to the beginning or the array.
        //if (Input.GetKeyDown(KeyCode.V))
        //{
        //    currentCameraIndex++;
        //    if (currentCameraIndex < cameras.Length)
        //    {
        //        cameras[currentCameraIndex - 1].gameObject.SetActive(false);
        //        cameras[currentCameraIndex].gameObject.SetActive(true);
        //    }
        //    else
        //    {
        //        cameras[currentCameraIndex - 1].gameObject.SetActive(false);
        //        currentCameraIndex = 0;
        //        cameras[currentCameraIndex].gameObject.SetActive(true);
        //    }
        //}
        #region No funciona
        //if (Input.GetKeyDown(KeyCode.V)) //or what ever key works for you
        //{
        //    if(currentCameraIndex > -1)
        //    {
        //        currentCameraIndex++;
        //        if (currentCameraIndex < cameras.Count)
        //        {
        //            cameras[currentCameraIndex - 1].gameObject.SetActive(false);
        //            cameras[currentCameraIndex].gameObject.SetActive(true);
        //        }
        //        else if(currentCameraIndex != -1 && currentCameraIndex < cameras.Count+1)
        //        {
        //            cameras[currentCameraIndex - 1].gameObject.SetActive(false);

        //            cameras[currentCameraIndex].gameObject.SetActive(true);
        //        }
        //        else
        //        {
        //            cameras[cameras.Count + 1].gameObject.SetActive(false);
        //            currentCameraIndex = 0;
        //            cameras[currentCameraIndex].gameObject.SetActive(true);
        //        }

        //    }
        //}
        #endregion
        if (Input.GetKeyUp(KeyCode.V))
        {
            mainCamera.enabled = !mainCamera.enabled;
            povCamera.enabled = !povCamera.enabled;
        }
    }
}

