using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float speed;
    private float turnSpeed;
    private float horizontalInput;
    private float forwardInput;
    public PlayerController()
    {
        this.speed = 20.0f;
        this.turnSpeed = 45.0f;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        this.horizontalInput = Input.GetAxis("Horizontal");
        this.forwardInput = Input.GetAxis("Vertical");
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) ||
            Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) ||
            Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) ||
            Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            // Move the vehicle forward based on vertical input
            this.transform.Translate(Vector3.forward * Time.deltaTime * this.speed * this.forwardInput);
            // Moves righ/left the vehicule
            //this.transform.Translate(Vector3.right * Time.deltaTime * this.turnSpeed * this.horizontalInput);
            //Better way to rotate realistic the vehicle turning to the left/right
            transform.Rotate(Vector3.up, Time.deltaTime * turnSpeed * horizontalInput);
        }
            
    }
}
