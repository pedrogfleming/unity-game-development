using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectsOffCam : MonoBehaviour
{
    private GameManager gameManager;
    public SoundManager soundManager;
    public static SoundEffectLibrary soundEffectLibrary;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        soundManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<SoundManager>();
        soundEffectLibrary = GameObject.Find("Sound Effects Library").GetComponent<SoundEffectLibrary>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        if (!other.gameObject.CompareTag("Bad"))
        {
            gameManager.livesLeft--;
            if(gameManager.livesLeft > 0)
            {
                soundManager.PlaySound(soundEffectLibrary.lostLiveSound);
            }            
        }
    }
}
