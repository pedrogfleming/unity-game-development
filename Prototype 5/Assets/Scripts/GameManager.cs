using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using static EventObject;

public class GameManager : MonoBehaviour
{
    public List<GameObject> targets;
    private float spawnRate = 1.0f;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI gameOverText;
    public GameObject displayInGame;
    public GameObject displayInPause;
    public GameObject titleScreen;
    public GameObject musicManager;
    public SoundManager soundManager;
    public EventsManager eventsManager;
    public static SoundEffectLibrary soundEffectLibrary;
    public Slider volumeSlider;
    public int gameSound;
    public Button restartButton;
    public bool isGameActive;
    public bool isGamePaused;
    private int difficulty;
    private int score;
    public int livesLeft;
    public float timeRound;

    public enum EPointValueTarget { Bad = -15, NormalTarget = 5, RandomBox = 15 };
    private int targetNormalPointValue = 5;
    private int targetBadPointValue = -15;
    private int targetRandomBox = 15;


    // Start is called before the first frame update
    void Start()
    {
        musicManager = GameObject.FindGameObjectWithTag("MusicManager");
        soundManager = musicManager.GetComponent<SoundManager>();
        soundEffectLibrary = GameObject.Find("Sound Effects Library").GetComponent<SoundEffectLibrary>();
        eventsManager = GameObject.FindGameObjectWithTag("Event Manager").GetComponent<EventsManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameActive)
        {
            timeRound += Time.deltaTime;
            if (!isGamePaused)
            {
                if (livesLeft < 1)
                {
                    GameOver();
                }
                livesText.text = $"Lives: {livesLeft}";
                if (Input.GetKeyDown(KeyCode.P))
                {
                    PauseGame();
                    isGamePaused = true;
                    displayInPause.SetActive(true);
                }
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                ResumeGame();
                isGamePaused = false;
                displayInPause.SetActive(false);
            }
        }
        else
        {
            timeRound = 0;
            //Musica del menu
            soundManager.PlayClipTrack(soundManager.tracks[0]);
        }
    }
    public int MusicDifficulty
    {
        get { return difficulty; }
        set
        {
            difficulty = value;
            soundManager.PlayClipTrack(soundManager.tracks[value]);
        }
    }
    IEnumerator SpawnTarget()
    {
        yield return new WaitForSeconds(spawnRate);
        while (isGameActive)
        {
            yield return new WaitForSeconds(spawnRate);
            int index = UnityEngine.Random.Range(0, targets.Count);
            GameObject auxObj = Instantiate(targets[index]);
            Target auxScript = auxObj.GetComponent<Target>();
            AssignPointValueTarget(targets[index].tag, auxScript);
            auxScript.normalTarget = true;
            soundManager.PlaySound(soundEffectLibrary.spawnTargetSound);
            if (timeRound > 7.5f)
            {
                //Random caotic event
                RandomCaoticBoxes();
                timeRound = 0;
            }
            if (auxObj.CompareTag("RandomEvent"))
            {
                GenerateRandomEvent(auxScript);

            }
        }
    }

    public void UpdateScore(int scoreToAdd)
    {
        score += scoreToAdd;
        scoreText.text = "Score " + score;
    }
    public void ChangeVolume(int v)
    {
        volumeSlider.value = v;
    }
    public void StartGame(int difficulty)
    {
        livesLeft = 3;
        isGameActive = true;
        spawnRate /= difficulty;
        StartCoroutine(SpawnTarget());
        score = 0;
        UpdateScore(0);
        displayInGame.gameObject.SetActive(true);
        titleScreen.gameObject.SetActive(false);
        MusicDifficulty = difficulty;
        soundManager.PlayClipTrack(soundManager.tracks[difficulty]);
    }
    public void GameOver()
    {
        gameOverText.gameObject.SetActive(true);
        isGameActive = false;
        restartButton.gameObject.SetActive(true);
        MusicDifficulty = 0;
        soundManager.PlaySound(soundEffectLibrary.gameOverSound);
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        soundManager.PlaySound(soundEffectLibrary.buttonOnClickSound);
    }
    private void PauseGame()
    {
        Time.timeScale = 0;
        soundManager._musicSource.Pause();
        soundManager.PlaySound(soundEffectLibrary.pauseSound);
    }

    private void ResumeGame()
    {
        Time.timeScale = 1;
        soundManager._musicSource.UnPause();
        soundManager.PlaySound(soundEffectLibrary.resumeSound);
    }
    private void AssignPointValueTarget(string tag, Target surpriseBoxScript)
    {
        switch (tag)
        {
            case "Bad":
                surpriseBoxScript.PointValue = targetBadPointValue;
                break;
            case "Normal Target":
                surpriseBoxScript.PointValue = targetNormalPointValue;
                break;
            case "RandomEvent":
                surpriseBoxScript.PointValue = targetRandomBox;
                break;
            default:
                break;
        }
    }
    #region Random Events Box

    private void GenerateRandomEvent(Target surpriseBoxScript)
    {
        int randomEvent = UnityEngine.Random.Range(1, 5);
        switch (randomEvent)
        {
            case 1:
                surpriseBoxScript.acRandomEvent = new Action(DoubleRateScore);
                surpriseBoxScript.typeEvent = ETypeEvent.DoubleRateScore;
                break;
            case 2:
                surpriseBoxScript.acRandomEvent = new Action(RandomCaoticBoxes);
                surpriseBoxScript.typeEvent = ETypeEvent.RandomCaoticBoxes;
                break;
            case 3:
                surpriseBoxScript.acRandomEvent = new Action(DestroyAllTargets);
                surpriseBoxScript.typeEvent = ETypeEvent.DestroyAll;
                break;
            case 4:
                surpriseBoxScript.acRandomEvent = new Action(SlowMotionTargets);
                surpriseBoxScript.typeEvent = ETypeEvent.SlowMotion;
                break;
        }
    }
    private void DoubleRateScore()
    {
        StartCoroutine(DelayDoubleRateScore());
    }
    private IEnumerator DelayDoubleRateScore()
    {
        targetNormalPointValue = 2 * (int)EPointValueTarget.NormalTarget;
        targetBadPointValue = (int)EPointValueTarget.Bad / 2;
        targetRandomBox = 2 * (int)EPointValueTarget.RandomBox;
        yield return new WaitForSeconds(3);
        targetNormalPointValue = (int)EPointValueTarget.NormalTarget;
        targetBadPointValue = (int)EPointValueTarget.Bad;
        targetRandomBox = (int)EPointValueTarget.RandomBox;
        eventsManager.eventFinished.Invoke(ETypeEvent.DoubleRateScore);
        yield return null;
        #region Flasheada con acceder a los archivos assets
        //string path = Path.Combine(Path.GetDirectoryName());
        //UnityEngine.Object[] arrayPrefabs = Resources.LoadAll("Assets/Prefabs");
        //foreach (var item in arrayPrefabs)
        //{
        //    GameObject auxGameObj = (GameObject)item;
        //    string tag = auxGameObj.tag;
        //    if (tag == "Bad" || tag == "Normal Target" || tag == "RandomEvent")
        //    {
        //        auxGameObj.GetComponent<Target>().pointValue *= 2;
        //    }
        //}
        //yield return new WaitForSeconds(3);
        //foreach (var item in arrayPrefabs)
        //{
        //    GameObject auxGameObj = (GameObject)item;
        //    string tag = auxGameObj.tag;
        //    if (tag == "Bad" || tag == "Normal Target" || tag == "RandomEvent")
        //    {
        //        auxGameObj.GetComponent<Target>().pointValue /= 2;
        //    }
        //}
        #endregion
    }
    private void RandomCaoticBoxes()
    {
        StartCoroutine(DelaySpawnRandomCaoticBoxes());
    }
    private IEnumerator DelaySpawnRandomCaoticBoxes()
    {
        for (int i = 0; i < 2; i++)
        {
            int index = UnityEngine.Random.Range(0, targets.Count);
            GameObject t = Instantiate(targets[index]);
            t.GetComponent<Target>().normalTarget = false;
            yield return new WaitForSeconds(1);
        }
        eventsManager.eventFinished.Invoke(ETypeEvent.RandomCaoticBoxes);
    }
    private void DestroyAllTargets()
    {
        if (isGameActive)
        {
            DelayDestroyingObjects();
            //StartCoroutine(DelayDestroyingObjects());
        }            
    }
    private void DelayDestroyingObjects()
    {
        List<GameObject> targets = new List<GameObject>();
        foreach (GameObject item in GameObject.FindObjectsOfType<GameObject>())
        {
            if (item.activeInHierarchy &&
                (item.CompareTag("Normal Target") ||
                 item.CompareTag("RandomEvent") ||
                item.CompareTag("Bad")))
            {
                targets.Add(item);
            }

        }
        eventsManager.eventFinished.Invoke(ETypeEvent.DestroyAll);
        //for (int i = 0;i < targets.ToArray().Length; i++)
        //{

        //    //yield return new WaitForSeconds(0.5f);
        //}
        foreach (GameObject item in targets)
        {
            Destroy(item);
        }
        //foreach (GameObject item in GameObject.FindObjectsOfType<GameObject>())
        //{
        //    //Target auxTarget = null;
        //    if (item.activeInHierarchy &&
        //        (item.CompareTag("Normal Target") ||
        //         item.CompareTag("RandomEvent") ||
        //        item.CompareTag("Bad")))
        //    {
        //        Destroy(item);    
        //        item.GetComponent<Target>().DestroyTarget();
        //    }
        //}        
    }
    private void SlowMotionTargets()
    {
        StartCoroutine(DelaySlowTime());        
    }
    private IEnumerator DelaySlowTime()
    {
        Time.timeScale = 0.5f;
        yield return new WaitForSeconds(3);
        Time.timeScale = 1;
        eventsManager.eventFinished.Invoke(ETypeEvent.SlowMotion);
    }
    #endregion


}
