using System.Collections;
using System.Collections.Generic;
using TMPro;
using System.Text;
using UnityEngine;
using System.Linq;
using System;
using static EventObject;

public class EventsManager : MonoBehaviour
{
    public Queue<EventObject> eventsStored;
    public TextMeshProUGUI displayTxtEvents;
    public List<EventObject> activeEvents;
    private GameManager gameManager;
    public Action<ETypeEvent> eventFinished;
    // Start is called before the first frame update
    void Start()
    {
        activeEvents = new List<EventObject>();
        eventsStored = new Queue<EventObject>();
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        eventFinished = RemoveEvent;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDisplayEventInfo();
        if(eventsStored.Count > 0)
        {
            EventObject evObj = eventsStored.Dequeue();
            if (IsValidEvent(evObj))
            {
                activeEvents.Add(evObj);
                evObj.TriggerEvent();
            }
        } 
    }
    /// <summary>
    /// Checks if the event to trigger already is active
    /// </summary>
    /// <param name="e"></param>
    /// <returns></returns>
    private bool IsValidEvent(EventObject e)
    {
        //return activeEvents.Any(x=> x.typeEvent == e.typeEvent);
        foreach (EventObject item in activeEvents)
        {
            if (e.typeEvent == item.typeEvent)
            {
                return false;
            }
        }
        return true;
    }
    private void UpdateDisplayEventInfo()
    {
        //Update the info display of current events
        StringBuilder sb = new StringBuilder();
        foreach (EventObject item in activeEvents)
        {
            sb.AppendLine(item.EventDescription);
        }
        displayTxtEvents.text = sb.ToString();
    }
    private void RemoveEvent(ETypeEvent etype)
    {
        foreach (var item in activeEvents)
        {
            if(item.typeEvent == etype)
            {
                activeEvents.Remove(item);
                break;
            }
        }            
    }

}
