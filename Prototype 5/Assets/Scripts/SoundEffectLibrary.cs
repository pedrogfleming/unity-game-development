using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectLibrary : MonoBehaviour
{
    public AudioClip buttonOnClickSound;
    public AudioClip pauseSound;
    public AudioClip resumeSound;
    public AudioClip gameOverSound;
    public AudioClip lostLiveSound;
    public AudioClip[] swipeSounds;
    public AudioClip explosionSound;
    public AudioClip[] goodItemsExplosions;
    public AudioClip spawnTargetSound;
    public static SoundEffectLibrary Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

}
