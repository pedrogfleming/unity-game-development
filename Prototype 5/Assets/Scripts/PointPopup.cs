using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointPopup : MonoBehaviour
{
    // Number of frames to completely interpolate between the 2 positions
    public int interpolationFramesCount = 45; 
    private Vector3 endPosition;
    private Vector3 startPosition;
    private float desiredDuration = 0.5f;
    private float elapsedTime;
   
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        endPosition = startPosition + new Vector3(0, 2, 0);
    }
    private void Update()
    {
        elapsedTime += Time.deltaTime;
        float percentageComplete = elapsedTime / desiredDuration;
        transform.position = Vector3.Lerp(startPosition, endPosition, percentageComplete);
        if(percentageComplete > 1)
        {
            Destroy(gameObject);
        }
    }

}
