using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventObject : ScriptableObject
{
    public enum ETypeEvent { RandomCaoticBoxes = 1, DestroyAll = 2, DoubleRateScore = 3, SlowMotion= 4};
    public string nameEvent;
    public Action acEvent;
    public bool triggered;
    public ETypeEvent typeEvent;
    
    //public EventObject(Action acEvent,ETypeEvent typeEvent)
    //{
    //    this.acEvent = acEvent;
    //    this.typeEvent = typeEvent;
    //}
    public string EventDescription
    {
        get
        {
            string st = String.Empty;
            switch (typeEvent)
            {
                case ETypeEvent.RandomCaoticBoxes:
                    st = "Watch out!!! Incoming Boxes";
                    break;
                case ETypeEvent.DestroyAll:
                    st = "Booooooom!!!";
                    break;
                case ETypeEvent.SlowMotion:
                    st = "Slowing down the beat...";
                    break;
                case ETypeEvent.DoubleRateScore:
                    st = "2x rate score, be fast!!!";
                    break;               
            }
            return st;
        }
    }
    public void TriggerEvent()
    {
        if(acEvent != null)
        {
            acEvent.Invoke();
            triggered = true;
        }
    }
}
