using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public AudioSource _musicSource;
    public AudioSource _effectSource;
    public List<AudioClip> tracks;
    public bool musicIsPlaying;
    public AudioClip _musicClipPlaying;
    private void Awake()
    {
        
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
    }
    /// <summary>
    /// Plays a music track
    /// </summary>
    /// <param name="clip"></param>
    public void PlayClipTrack(AudioClip clip)
    {
        if(_musicClipPlaying == clip)
        {
            return;
        }
        else
        {
            _musicSource.Stop();
            _musicSource.GetComponent<AudioSource>().clip = clip;
            _musicClipPlaying = clip;
            _musicSource.Play();
            musicIsPlaying = true;
        }
    }
    /// <summary>
    /// Plays an audio effect
    /// </summary>
    /// <param name="clip"></param>
    public void PlaySound(AudioClip clip)
    {
        _effectSource.PlayOneShot(clip);
    }
    public void ChangeMasterVolume(float value)
    {
        AudioListener.volume = value;
    }
    public void MuteMusic()
    {
        _musicSource.mute = !_musicSource.mute;
    }
    public void UnMuteMusic()
    {
        _musicSource.mute=false;
    }
}