using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    private Vector2 initialPos;
    private Vector3 finalPos;
    public TrailRenderer trailRenderer;
    public bool isSwiping;
    public float distance = 10;
    public BoxCollider boxCollider;
    private GameManager gameManager;
    public SoundManager soundManager;
    public static SoundEffectLibrary soundEffectLibrary;
    public CameraShaker camShaker;
    private void Start()
    {
        //trailRenderer = GetComponent<TrailRenderer>();
        //trailRenderer.gameObject.SetActive(false);
        soundManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<SoundManager>();
        soundEffectLibrary = GameObject.Find("Sound Effects Library").GetComponent<SoundEffectLibrary>();

        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        gameObject.transform.position = Input.mousePosition;
        isSwiping = false;
    }
    void Update()
    {
        //gameObject.transform.position = Input.mousePosition;
        Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 pos = r.GetPoint(distance);
        transform.position = pos;


        if(gameManager.isGameActive && !gameManager.isGamePaused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //initialPos = Input.mousePosition;
                //trailRenderer.transform.position = initialPos;
                isSwiping = true;
                trailRenderer.gameObject.SetActive(true);
                boxCollider.enabled = true;
                int randomSwipeSound = Random.Range(0, soundEffectLibrary.swipeSounds.Length);
                soundManager.PlaySound(soundEffectLibrary.swipeSounds[randomSwipeSound]);

            }
            if (Input.GetMouseButtonUp(0))
            {
                //Calculate(Input.mousePosition);
                //finalPos = Input.mousePosition;
                isSwiping = false;
                boxCollider.enabled = false;
                trailRenderer.gameObject.SetActive(false);
            }
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (isSwiping)
        {            
            if (other.gameObject.CompareTag("Bad"))
            {
                soundManager.PlaySound(soundEffectLibrary.explosionSound);
                StartCoroutine(camShaker.Shake(0.15f,0.4f));
            }
            else
            {
                int randomIndex = Random.Range(0, soundEffectLibrary.goodItemsExplosions.Length);
                soundManager.PlaySound(soundEffectLibrary.goodItemsExplosions[randomIndex]);
            }
            Destroy(other.gameObject);
        }
    }
    //void Calculate(Vector3 endPosition)
    //{
    //    float disX = Mathf.Abs(initialPos.x - endPosition.x);
    //    float disY = Mathf.Abs(initialPos.y - endPosition.y);
    //    if (disX > 0 || disY > 0)
    //    {
    //        if (disX > disY)
    //        {
    //            if (initialPos.x > endPosition.x)
    //            {
    //                //Debug.Log("Left");
    //            }
    //            else
    //            {
    //                //Debug.Log("Right");
    //            }
    //        }
    //        else
    //        {
    //            if (initialPos.y > endPosition.y)
    //            {
    //                //Debug.Log("Down");
    //            }
    //            else
    //            {
    //                //Debug.Log("Up");
    //            }
    //        }
    //    }
    //}
}
