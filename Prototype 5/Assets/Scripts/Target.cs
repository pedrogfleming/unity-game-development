using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static EventObject;

public class Target : MonoBehaviour
{
    private Rigidbody targetRb;
    private GameManager gameManager;
    public ParticleSystem explosionParticle;
    [SerializeField] private Transform pfDamagePopup;
    private SoundManager soundManager;
    public static SoundEffectLibrary soundEffectLibrary;
    private int pointValue;
    private float minSpeed = 12;
    private float maxSpeed = 16;
    private float maxTorque = 10;
    private float xRange = 10;
    private float ySpawnPos = -2;
    public bool normalTarget;
    public Action acRandomEvent;
    public ETypeEvent typeEvent;

    public ETypeOfTarget typeTarget;
    public enum ETypeOfTarget{ normalBox = 0,surpriseBox = 1};
    private enum ESpawnSide {left = -1,up = 0,right = 1};
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        soundManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<SoundManager>();
        soundEffectLibrary = GameObject.Find("Sound Effects Library").GetComponent<SoundEffectLibrary>();
        targetRb = GetComponent<Rigidbody>();
        if(normalTarget)
        {
            transform.position = RandomSpawnPos();
            targetRb.AddForce(RandomForce(Vector3.up), ForceMode.Impulse);
        }
        else
        {
            transform.position = RandomSpawnPosCaotic();
        }        
        targetRb.AddTorque(RandomTorque(),
            RandomTorque(),
            RandomTorque(),
            ForceMode.Impulse);

    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public int PointValue { get { return pointValue; } set { pointValue = value; } }

    private void OnTriggerEnter(Collider other)
    {
        if (gameManager.isGameActive && !gameManager.isGamePaused && other.CompareTag("MousePlayer"))
        {
            DestroyTarget();
        }
    }
    public void DestroyTarget()
    {
        Transform gameObj = Instantiate(pfDamagePopup, transform.position, Quaternion.identity);
        TextMeshPro txtPoint = gameObj.GetComponent<TextMeshPro>();
        txtPoint.text = pointValue.ToString();
        if (pointValue < 0)
        {
            txtPoint.color = Color.red;
        }
        Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
        gameManager.UpdateScore(pointValue);
        if (this.CompareTag("RandomEvent") && acRandomEvent != null)
        {
            //Invoco el powerUP           
            //acRandomEvent.Invoke();
            EventsManager eventMg = GameObject.FindGameObjectWithTag(
                "Event Manager").GetComponent<EventsManager>();
            EventObject eventObj = ScriptableObject.CreateInstance<EventObject>();
            eventObj.acEvent = acRandomEvent;
            eventObj.typeEvent = typeEvent;
            eventMg.eventsStored.Enqueue(eventObj);
            if (typeEvent != ETypeEvent.DestroyAll)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void RandomPowerUp()
    {

    }
    private Vector3 RandomForce(Vector3 direction)
    {
        return direction * UnityEngine.Random.Range(minSpeed,maxSpeed);
    }
    private float RandomTorque()
    {
        return UnityEngine.Random.Range(-maxTorque,maxTorque);
    }
    private Vector3 RandomSpawnPos()
    {
        return new Vector3(UnityEngine.Random.Range(-xRange, xRange), ySpawnPos);
    }
    private Vector3 RandomSpawnPosCaotic()
    {
        //-1 Left, 0 Up, 1 Right
        int randomSide = UnityEngine.Random.Range(((int)ESpawnSide.left), (int)ESpawnSide.right);
        float xPos = 17;
        float ypos = 10;
        switch (randomSide)
        {
            case -1:
                xPos = -17;
                ypos = 10;
                targetRb.AddForce(RandomForce(Vector3.right), ForceMode.Impulse);
                break;
            case 0:
                ypos = 15;
                xPos = UnityEngine.Random.Range(-xRange, xRange);
                break;
            case 1:
                xPos = 17;
                ypos = 10;
                targetRb.AddForce(RandomForce(Vector3.left), ForceMode.Impulse);
                break;
            default:
                break;
        }
        return new Vector3(xPos, ypos,0);
    }
}
