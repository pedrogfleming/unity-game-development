using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DifficultyButton : MonoBehaviour
{
    private Button button;
    private GameManager gameManager;
    private SoundManager soundManager;
    public static SoundEffectLibrary soundEffectLibrary;
    public int difficulty;

    // Start is called before the first frame update
    void Start()
    {
        soundManager = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<SoundManager>();
        soundEffectLibrary = GameObject.Find("Sound Effects Library").GetComponent<SoundEffectLibrary>();
        button = GetComponent<Button>();
        button.onClick.AddListener(SetDifficulty);
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void SetDifficulty()
    {
        soundManager.PlaySound(soundEffectLibrary.buttonOnClickSound);
        gameManager.StartGame(difficulty);
    }
}
