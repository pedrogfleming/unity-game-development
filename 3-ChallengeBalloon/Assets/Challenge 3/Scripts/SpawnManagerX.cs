﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManagerX : MonoBehaviour
{
    public GameObject[] objectPrefabs;
    private float spawnDelay = 2;
    private float spawnInterval = 1.5f;
    private float xSpawnPos = 30;
    private float ySpawnPos;
    private float zSpawnPos = 0;
    private PlayerControllerX playerControllerScript;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObjects", spawnDelay, spawnInterval);
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerControllerX>();
    }
    // Spawn obstacles
    void SpawnObjects ()
    {
        // If game is still active, spawn new object
        if (!playerControllerScript.gameOver)
        {
            // Set random spawn location and random object index
            ySpawnPos = Random.Range(5, 15);
            Vector3 spawnLocation = new Vector3(xSpawnPos, ySpawnPos, zSpawnPos);
            int index = Random.Range(0, objectPrefabs.Length);
            Instantiate(objectPrefabs[index], spawnLocation, objectPrefabs[index].transform.rotation);
        }
    }
}
