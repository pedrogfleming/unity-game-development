using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] animalPrefabs;
    public GameObject healtBar;
    private float spawnRangeX = 20.0f;
    private float spawnPosZ = 20.0f;
    private float startDelay = 2;
    private float spawnIntervals = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnRandomAnimal", startDelay, spawnIntervals);
    }

    // Update is called once per frame
    void Update()
    {
    }
    void SpawnRandomAnimal()
    {
        int spawmRandomSide = Random.Range(-1, 2);
        int animalIndex = Random.Range(0, animalPrefabs.Length);
        Vector3 spawnPos = Vector3.zero;
        switch (spawmRandomSide)
        {
            case -1:
                spawnPos = new Vector3(-spawnRangeX, 0, Random.Range(-10.0f,30.0f));
                InstantiateAnimalPrefab(animalIndex, spawnPos, Quaternion.Euler(0, 90, 0));
                break;
            case 0:
                spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);
                InstantiateAnimalPrefab(animalIndex, spawnPos, Quaternion.Euler(0, 180, 0));
                break;
            case 1:
                spawnPos = new Vector3(spawnRangeX, 0, Random.Range(-10.0f, 30.0f));
                InstantiateAnimalPrefab(animalIndex, spawnPos, Quaternion.Euler(0, -90, 0));
                break;
            default:
                break;
        }
        //Vector3 spawnPos = new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, spawnPosZ);

        //if(spawnPos != Vector3.zero)
        //{
        //    Instantiate(animalPrefabs[animalIndex], spawnPos, animalPrefabs[animalIndex].transform.rotation);
        //}
    }
    private void InstantiateAnimalPrefab(int animalIndex, Vector3 spawnPosition, Quaternion rotationAnimal)
    {
        Instantiate(animalPrefabs[animalIndex], spawnPosition, rotationAnimal);
    }
}
 