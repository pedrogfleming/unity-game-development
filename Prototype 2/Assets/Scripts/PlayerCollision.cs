using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    PlayerScore playerScore;
    // Start is called before the first frame update
    void Start()
    {
        
        playerScore = GameObject.FindGameObjectWithTag("PlayerStats").GetComponent<PlayerScore>();
        Debug.Log($"Player�s Lives = {playerScore.lives}");
    }

    // Update is called once per frame
    void Update()
    {
            
    }
    void OnTriggerEnter(Collider other)
    {
        gameObject.GetComponentInChildren<SliderBarDecrease>().DecreaseSliderValue();
        playerScore.LoseLife();

    }
}

