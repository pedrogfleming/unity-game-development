using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class DestroyOutOfBounds : MonoBehaviour
{
    private float topZBound = 30.0f;
    private float bottomZBound = -10.0f;
    private float horizontalXBound = 20.0f;
    PlayerScore playerScore;
    // Start is called before the first frame update
    void Start()
    {
        playerScore = GameObject.FindGameObjectWithTag("PlayerStats").GetComponent<PlayerScore>();
    }
    // Update is called once per frame
    void Update()
    {
        if(!IsInRange(transform.position.z,bottomZBound,topZBound) ||
            !IsInRange(transform.position.x, -horizontalXBound, horizontalXBound))
        {
            Destroy(gameObject);
            //if(gameObject.tag != "pizza")
            //{
            //    playerScore.LoseLife();
            //}
        }
    }
    private bool IsInRange(float number,float min,float max)
    {
        return number >= min && number <= max;
    }
}
