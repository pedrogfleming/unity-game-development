using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float horinzontalInput;
    public float zInput;
    private float speed = 20.0f;
    private float xRange = 20.0f;
    private float maxZRange = 25.0f;
    private float minZRange = -5.0f;
    public GameObject projectilePrefab;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            //Launch a projectile from the player
            Instantiate(projectilePrefab,transform.position,projectilePrefab.transform.rotation);
        }
        //Keep the player in bounds
       
        horinzontalInput = Input.GetAxis("Horizontal");
        zInput = Input.GetAxis("Vertical");
        if(horinzontalInput != 0)
        {
            transform.Translate(Vector3.right * horinzontalInput * speed * Time.deltaTime);
        }
        if (zInput != 0)
        {
            transform.Translate(Vector3.forward * zInput * speed * Time.deltaTime);
        }        
        keepPlayerInBounds();
    }
    /// <summary>
    /// Keeps the player in certan bounds in x and z values
    /// </summary>
    void keepPlayerInBounds()
    {        
        if (transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }
        if (transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }
        if (transform.position.z < minZRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, minZRange);
        }
        else if (transform.position.z > maxZRange)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, maxZRange);
        }
    }
}
