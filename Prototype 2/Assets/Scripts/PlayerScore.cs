using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScore : MonoBehaviour
{
    public int score;
    public int lives = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (lives == 0)
        {
            Debug.Log("GameOver");
            Debug.Break();
        }
    }
    public void LoseLife()
    {
        lives--;
        Debug.Log($"Player�s Lives = {lives}");
    }
    public void UpScore()
    {
        score++;
        Debug.Log($"Player Score = {score}");
    }
}
