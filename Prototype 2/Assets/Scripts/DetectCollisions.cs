using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectCollisions : MonoBehaviour
{
    PlayerScore playerScore;
    // Start is called before the first frame update
    void Start()
    {
        playerScore = GameObject.FindGameObjectWithTag("PlayerStats").GetComponent<PlayerScore>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponentInChildren<SliderBarDecrease>().DecreaseSliderValue();
        
        if(other.gameObject.GetComponentInChildren<SliderBarDecrease>().slider.value <= 0)
        {
            Destroy(other.gameObject);
            playerScore.UpScore();
        }
        Destroy(gameObject);  
    }
}
