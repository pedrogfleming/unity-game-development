using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketHoming : MonoBehaviour
{
    private GameObject[] enemies;
    public GameObject rocket;
    public GameObject targetEnemy;
    private float rocketStrengh = 13;
    public Action delTargetDestroyed;
    private float xBound = 20;
    private float yBound = 10;
    private float zBound = 20;
    Projectiles typeOfProjectile = Projectiles.Rocket;
    public float speed = 10;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        DestroyProjectileOffLimits();
        if (targetEnemy == null && delTargetDestroyed != null)
        {
            delTargetDestroyed.Invoke();
        }
        //transform.LookAt(targetEnemy.transform);
    }
    private void OnTriggerEnter(Collider other)
    {
        //if(other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Player"))
        if(other.tag == targetEnemy.tag)
        {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            Vector3 awayFromRocket = (other.gameObject.transform.position - transform.position);
            rb.AddForce(awayFromRocket * rocketStrengh, ForceMode.Impulse);
            Destroy(gameObject);
            if(other.gameObject == targetEnemy && delTargetDestroyed != null)
            {
                delTargetDestroyed.Invoke();
            }
        }
    }
    private void DestroyProjectileOffLimits()
    {
        if(gameObject.transform.position.x > xBound || gameObject.transform.position.x < -xBound ||
           gameObject.transform.position.y > yBound || gameObject.transform.position.y < -yBound ||
           gameObject.transform.position.z > zBound || gameObject.transform.position.z < -zBound)
        {
            Destroy(gameObject);
        }
    }
}
