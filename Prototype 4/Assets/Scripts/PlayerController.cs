using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    public float speed = 5.0f;
    private GameObject focalPoint;
    private float powerupStrength = 15.0f;
    private float jumpForce = 30;
    private float smashingForce = 10;
    private bool smashing;
    public GameObject powerupIndicatorStrenght;
    public GameObject powerupIndicatorRocketLauncher;
    public GameObject powerupIndicatorSmash;
    private Vector3 positionPowerupIndicators;
    private float xBound = 20;
    private float yBound = 20;
    private float zBound = 20;

    public bool hasPowerup = false;
    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        focalPoint = GameObject.Find("Focal Point");
        
    }

    // Update is called once per frame
    void Update()
    {
        positionPowerupIndicators = transform.position + new Vector3(0, -0.5f, 0);
        playerFall();
        float fowardInput = Input.GetAxis("Vertical");
        playerRb.AddForce(focalPoint.transform.forward * fowardInput * speed,ForceMode.Acceleration);
        powerupIndicatorStrenght.transform.position = positionPowerupIndicators;
        powerupIndicatorRocketLauncher.transform.position = positionPowerupIndicators;
        powerupIndicatorSmash.transform.position = positionPowerupIndicators;
        if(powerupIndicatorSmash.activeInHierarchy && Input.GetKeyDown(KeyCode.Space))
        {
            playerRb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
            
        }
        if (transform.position.y > 7)
        {
            playerRb.AddForce(Vector3.down * jumpForce, ForceMode.VelocityChange);
            smashing = true;
        }
        if (smashing && transform.position.y < 1)
        {
            SmashEnemies();
        }
    }
    IEnumerator PowerupCountdownRoutine(Action<bool> ac) 
    { 
        yield return new WaitForSeconds(7);
        hasPowerup = false;
        //Deactivate the powerup indicator
        ac.Invoke(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Powerup"))
        {
            ActivatePowerup(other.gameObject);
        }                
    }
    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
        switch (collision.gameObject.tag)
        {
            case "Enemy":
                if (powerupIndicatorStrenght.activeInHierarchy)
                {
                    //enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                    //Vector3 awayFromPlayer = (collision.gameObject.transform.position - transform.position);
                    //enemyRigidbody.AddForce(awayFromPlayer * powerupStrength, ForceMode.Impulse);
                    RepelPhysicBody(collision.gameObject, this.gameObject, powerupStrength, ForceMode.Impulse);
                }
                break;
            case "EnemyT2":
                //enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                //Vector3 awayFromEnemy = (transform.position - collision.gameObject.transform.position);
                //playerRb.AddForce(awayFromEnemy * 2, ForceMode.Impulse);
                RepelPhysicBody(this.gameObject, collision.gameObject, 2, ForceMode.Impulse);
                break;
        }
    }
    private static void RepelPhysicBody(GameObject target,GameObject fromObject,float forceApplied,ForceMode fMode, float angle = 0)
    {
        Rigidbody rbTarget = target.GetComponent<Rigidbody>();
        Vector3 awayFromObject= (target.transform.position - fromObject.transform.position);
        awayFromObject.y = awayFromObject.y + angle;
        rbTarget.AddForce(awayFromObject * forceApplied, fMode);
    }
    private void SmashEnemies()
    { 
        List<GameObject> enemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
        foreach (GameObject enemy in enemies)
        {
            float distance = Vector3.Distance(transform.position, enemy.transform.position);
            if (Vector3.Distance(transform.position, enemy.transform.position) < 6)
            {
                //Rigidbody enemyRigidbody = enemy.gameObject.GetComponent<Rigidbody>();
                //enemyRigidbody = enemy.gameObject.GetComponent<Rigidbody>();
                //Vector3 awayFromPlayer = (enemy.gameObject.transform.position - transform.position);
                //Vector3 angle = new Vector3(0, 2.5f, 0);
                //enemyRigidbody.AddForce((awayFromPlayer+angle) * smashingForce, ForceMode.Impulse);
                RepelPhysicBody(enemy.gameObject, this.gameObject, smashingForce, ForceMode.Impulse, 2.5f);
            }
        }
        smashing = false;        
    }
    private void ActivatePowerup(GameObject gObj)
    {
        Action<bool>acVisibilityPowerup;
        switch(gObj.name)
        {
            case "Powerup(Clone)":
                acVisibilityPowerup = powerupIndicatorStrenght.SetActive;
                break;
            case "Powerup2(Clone)":
                acVisibilityPowerup = powerupIndicatorRocketLauncher.SetActive;
                break;
            case "Powerup3(Clone)":
                acVisibilityPowerup = powerupIndicatorSmash.SetActive;
                break;
            default:
                return;
        }
        hasPowerup = true;
        Destroy(gObj);
        acVisibilityPowerup.Invoke(true);
        StartCoroutine(PowerupCountdownRoutine(acVisibilityPowerup));        
    }    
    private void playerFall()
    {
        if (gameObject.transform.position.x > xBound || gameObject.transform.position.x < -xBound ||
            gameObject.transform.position.y > yBound || gameObject.transform.position.y < -yBound ||
            gameObject.transform.position.z > zBound || gameObject.transform.position.z < -zBound)
        {
            Destroy(gameObject);
            Debug.Log("Game Over");
            Debug.Break();
        }
    }
}
