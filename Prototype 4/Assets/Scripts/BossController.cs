using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public enum EBossType {Spawner = 1,Rocketer = 2,Agile = 3};
    public EBossType bossType;
    private SpawnManager spawnManagerScript;
    public GameObject rocketPrefab;
    private Enemy enemyScript;
    // Start is called before the first frame update
    private void Awake()
    {
        spawnManagerScript = GameObject.Find("Spawn Manager").GetComponent<SpawnManager>();
        enemyScript = gameObject.GetComponent<Enemy>();
    }
    void Start()
    {
        bossType = BossType;
        InvokeRepeating("BossPower", 1, 4);
    }

    // Update is called once per frame
    void Update()
    {        
    }
    public EBossType BossType 
    {
        get
        {
            int typeOfBoss = UnityEngine.Random.Range(1, 4);
            switch (typeOfBoss)
            {
                case 0:
                    //Spawner Boss
                    bossType = EBossType.Spawner;
                    gameObject.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
                    break;
                case 1:
                    //Rocketer Boss
                    bossType = EBossType.Rocketer;
                    gameObject.GetComponent<Renderer>().material.color = new Color(0, 1, 0);
                    break;
                case 2:
                    //Agile Boss
                    bossType = EBossType.Agile;
                    gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 1);
                    break;
                default:
                    //bossType = EBossType.Spawner;
                    break;
            }
            return bossType;
        }           
    }
    private void BossPower()
    {
        switch (bossType)
        {
            case EBossType.Spawner:
                BossSpawns();
                break;
            case EBossType.Rocketer:
                StartCoroutine(LaunchProjectiles(0.25f));
                break;
            case EBossType.Agile:
                enemyScript.speed = 60;
                CancelInvoke("BossPower");

                break;
            default:
                break;
        }
    }
    private IEnumerator LaunchProjectiles(float t = 0.5f)
    {
        if(gameObject.activeInHierarchy)
        {
            for (int i = 0; i < 3; i++)
            {                                
                GameObject rocketLaunched = Instantiate(rocketPrefab,
                   transform.position,
                   rocketPrefab.transform.rotation);
                RocketHoming scriptRocket = rocketLaunched.GetComponent<RocketHoming>();

                rocketLaunched.GetComponent<RocketHoming>().targetEnemy = spawnManagerScript.player;
                Vector3 lookDirection = (rocketLaunched.GetComponent<RocketHoming>().targetEnemy.transform.position - transform.position).normalized;
                rocketLaunched.GetComponent<Rigidbody>().AddForce(lookDirection * scriptRocket.speed, ForceMode.VelocityChange);
                //transform.LookAt(spawnManagerScript.player.transform);
                yield return new WaitForSeconds(t);
            }
        }
    }
    //private IEnumerator CoolDownLaunch(float t = 2)
    //{
    //    yield return new WaitForSeconds(t);
    //}
    private void BossSpawns()
    {        
        if(gameObject.activeInHierarchy)
        {
            spawnManagerScript.SpawnEnemyWave(2);             
        }
    }
}
