using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum Projectiles { Rocket };
public class SpawnManager : MonoBehaviour
{
    
    public GameObject[] enemiesPrefabs;
    public GameObject[] powerupsPrefabs;
    public GameObject[] projectilesPrefabs;
    public GameObject[] bossesPrefabs;
    private GameObject boss;
    public GameObject player;
    private PlayerController playerControllerScript;
    private List <GameObject> enemies;
    public List<GameObject> targetedEnemies;
    public GameObject rocket;
    private float speedRocket = 10;

    private float spawnRange = 9.0f;
    public int waveNumber = 1;
    // Start is called before the first frame update
    private void Awake()
    {
        playerControllerScript = player.GetComponent<PlayerController>();
    }
    void Start()
    {
        SpawnEnemyWave(waveNumber);
        SpawnPowerup();
        //playerControllerScript = player.GetComponent<PlayerController>();
        //Instantiate(powerupPrefab, GenerateSpawnPosition(), powerupPrefab.transform.rotation);

    }
    // Update is called once per frame
    void Update()
    {       
        CountEnemiesAlive();
        if (enemies.Count == 0)
        {
            waveNumber++;
            if (waveNumber % 3 != 0)
            {
                SpawnEnemyWave(waveNumber);
            }
            else
            {
                //Boss round
                boss = Instantiate(bossesPrefabs[0], GenerateSpawnPosition(), bossesPrefabs[0].transform.rotation);
            }
            SpawnPowerup();
        }
        if (playerControllerScript.powerupIndicatorRocketLauncher.activeInHierarchy && Input.GetKeyDown(KeyCode.E) && enemies.Count > 0)
        {            
            SpawnProjectiles(0);
        }
    }
    public void SpawnEnemyWave(int enemiesToSpawn)
    {

        for (int i = 0; i < enemiesToSpawn; i++)
        {
            int indexPrefab = UnityEngine.Random.Range(0, enemiesPrefabs.Length);
            Instantiate(enemiesPrefabs[indexPrefab], GenerateSpawnPosition(), enemiesPrefabs[indexPrefab].transform.rotation);
        }
    }
    private void SpawnPowerup()
    {
        int indexPrefab = UnityEngine.Random.Range(0, powerupsPrefabs.Length);
        if (GameObject.FindGameObjectsWithTag("Powerup").Length == 0 && !playerControllerScript.hasPowerup)
        {
            Instantiate(powerupsPrefabs[indexPrefab], GenerateSpawnPosition(), powerupsPrefabs[indexPrefab].transform.rotation);
        }

    }
    private void SpawnProjectiles(int indexProjectilePrefabs)
    {
        foreach (GameObject item in enemies)
        {
            GameObject rocketLaunched = Instantiate(projectilesPrefabs[indexProjectilePrefabs],
            player.transform.position,
            projectilesPrefabs[indexProjectilePrefabs].transform.rotation);
            RocketHoming scriptRocket = rocketLaunched.GetComponent<RocketHoming>();
            targetedEnemies.Add(item);
            scriptRocket.delTargetDestroyed = new Action(
                () => targetedEnemies.Remove(item));
            rocketLaunched.GetComponent<RocketHoming>().targetEnemy = item;

            Vector3 lookDirection = (item.transform.position - player.transform.position).normalized;
            rocketLaunched.GetComponent<Rigidbody>().AddForce(lookDirection * speedRocket, ForceMode.VelocityChange);
        }
        //for (int i = 0; i < enemies.Length; i++)
        //{
        //    Instantiate(projectilesPrefabs[indexProjectilePrefabs], player.transform.position, projectilesPrefabs[indexProjectilePrefabs].transform.rotation);
        //    Vector3 lookDirection = (enemies[i].transform.position - transform.position).normalized;

        //    Rigidbody rigidBodyRocket = enemies[i].GetComponent<Rigidbody>();
        //    rigidBodyRocket.AddForce(lookDirection * speedRocket, ForceMode.Impulse);
        //}
    }
    private Vector3 GenerateSpawnPosition()
    {
        float spawnPosX = UnityEngine.Random.Range(-spawnRange, spawnRange);
        float spawnPosZ = UnityEngine.Random.Range(-spawnRange, spawnRange);
        return new Vector3(spawnPosX, 0, spawnPosZ);
    }
    private void CountEnemiesAlive()
    {
        enemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
    }
}
