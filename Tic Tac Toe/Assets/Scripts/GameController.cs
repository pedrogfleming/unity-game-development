using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public enum EGameMode { pvp=0,pvc=1};
public class Player
{
    public Image panel;
    public Text text;
    public Button button;
}

[System.Serializable]
public class  PlayerColor
{
    public Color panelColor;
    public Color textColor;
}
public class GameController : MonoBehaviour
{
    public Text[] buttonList;
    public List<string> marks;
    private string playerSide;
    public GameObject gameOverPanel;
    public GameObject restartButton;
    public GameObject startInfo;
    public Text gameOverText;
    private int moveCount;
    public Player playerX;
    public Player playerO;
    public GameObject gameobjX;
    public GameObject gameobjO;
    public PlayerColor activePlayerColor;
    public PlayerColor inactivePlayerColor;
    public Dictionary<int, bool> legalMovements;
    public ComputerPlayer computerPlayer;
    public EGameMode gameMode;    
    void Awake()
    {
        gameobjX = GameObject.FindGameObjectWithTag("PlayerX");
        gameobjO = GameObject.FindGameObjectWithTag("PlayerO");
        playerX = new Player();
        playerO = new Player();
        playerX.text = gameobjX.GetComponentInChildren<Text>();
        playerX.button = gameobjX.GetComponent<Button>();
        playerX.panel = gameobjX.GetComponent<Image>();
        playerO.text = gameobjO.GetComponentInChildren<Text>();
        playerO.button = gameobjO.GetComponent<Button>();
        playerO.panel = gameobjO.GetComponent<Image>();

        computerPlayer = ScriptableObject.CreateInstance<ComputerPlayer>();
        restartButton.SetActive(false);
        moveCount = 0;
        SetGameControllerReferenceOnButtons();
        gameOverPanel.SetActive(false);
        legalMovements = new Dictionary<int, bool>();
        marks = new List<string>();
    }
    private void Update()
    {
        if(gameMode == EGameMode.pvc && computerPlayer.isPcTurn)
        {
            Cursor.visible = false;            
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
    void ComputerGetLegalMoves()
    {
        marks.Clear();
        legalMovements.Clear();        
        for (int i = 0; i < buttonList.Length; i++)
        {            
            legalMovements.Add(i,buttonList[i].GetComponentInParent<GridSpace>().button.interactable);
            marks.Add(buttonList[i].text);
        }
    }
    void CalculateRiskingMovements()
    {
        for (int i = 0; i < legalMovements.Count; i++)
        {
            
        }
    }
    public void SetGameMode(int m)
    {
        gameMode = (EGameMode)m;
    }
    public IEnumerator ComputerMoves()
    {
        float t = Random.Range(1, 2);        
        yield return new WaitForSeconds(t);
        //In case the random move weren�t possible, it will calculate again
        do
        {
            ComputerGetLegalMoves();
            int randomMove = Random.Range(1, legalMovements.Count);
            Dictionary<int, bool> auxD = new Dictionary<int, bool>();
            legalMovements.TryGetValue(randomMove, out bool enabled);
            if (enabled)
            {
                buttonList[randomMove].GetComponentInParent<GridSpace>().SetSpace();
                break;
            }
        }while (true);
        computerPlayer.isPcTurn = false;
    }
    void SetGameControllerReferenceOnButtons()
    {
        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<GridSpace>().SetGameControllerReference(this);
        }
    }
    public string GetPlayerSide()
    {
        return playerSide;
    }
    void SetPlayerColors(Player newPlayer, Player oldPlayer)
    {
        newPlayer.panel.color = activePlayerColor.panelColor;
        newPlayer.text.color = activePlayerColor.textColor;
        oldPlayer.panel.color = inactivePlayerColor.panelColor;
        oldPlayer.text.color = inactivePlayerColor.textColor;
    }
    void ChangeSides()
    {
        playerSide = (playerSide == "X") ? "O" : "X"; // Note: Capital Letters for "X" and "O"
        if (playerSide == "X")
        {
            SetPlayerColors(playerX, playerO);
        }
        else
        {
            SetPlayerColors(playerO, playerX);
        }
    }
    void SetGameOverText(string value)
    {
        gameOverPanel.SetActive(true);
        gameOverText.text = value;
    }
    public void RestartGame()
    {
        moveCount = 0;
        gameOverPanel.SetActive(false);        
        for (int i = 0; i < buttonList.Length; i++)
        {
            //Removing the X or 0 in the grid
            buttonList[i].text = "";
        }
        restartButton.SetActive(false);
        SetPlayerButtons(true);
        SetPlayerColorsInactive();
        startInfo.SetActive(true);
        playerSide = string.Empty;

    }
    public void StartGame()
    {
        if(!string.IsNullOrEmpty(playerSide))
        {
            SetBoardInteractable(true);
            SetPlayerButtons(false);
            startInfo.SetActive(false);
        }
    }
    void SetPlayerButtons(bool toggle)
    {
        playerX.button.interactable = toggle;
        playerO.button.interactable = toggle;

    }
    void SetPlayerColorsInactive() 
    { 
        playerX.panel.color = inactivePlayerColor.panelColor;
        playerX.text.color = inactivePlayerColor.textColor;
        playerO.panel.color = inactivePlayerColor.panelColor;
        playerO.text.color = inactivePlayerColor.textColor; 
    }
    public void SetStartingSide(string startingSide)
    {
        playerSide = startingSide;
        if (playerSide == "X")
        {
            SetPlayerColors(playerX, playerO);
            computerPlayer.side = "O";
        }
        else
        {
            SetPlayerColors(playerO, playerX);
            computerPlayer.side = "X";
        }        
    }
    void SetBoardInteractable(bool toggle) 
    {
        for (int i = 0; i<buttonList.Length; i++) 
        { 
             buttonList[i].GetComponentInParent<Button>().interactable = toggle; 
        }
    }
    public void EndTurn()
    {
        moveCount++;
        if (buttonList[0].text == playerSide && buttonList[1].text == playerSide && buttonList[2].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (buttonList[3].text == playerSide && buttonList[4].text == playerSide && buttonList[5].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (buttonList[6].text == playerSide && buttonList[7].text == playerSide && buttonList[8].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (buttonList[0].text == playerSide && buttonList[3].text == playerSide && buttonList[6].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (buttonList[1].text == playerSide && buttonList[4].text == playerSide && buttonList[7].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (buttonList[2].text == playerSide && buttonList[5].text == playerSide && buttonList[8].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (buttonList[0].text == playerSide && buttonList[4].text == playerSide && buttonList[8].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (buttonList[2].text == playerSide && buttonList[4].text == playerSide && buttonList[6].text == playerSide)
        {
            GameOver(playerSide);
        }
        else if (moveCount >= 9)
        {
            GameOver("draw");
        }
        else
        {
            ChangeSides();
            switch (gameMode)
            {
                case EGameMode.pvp:
                    break;
                case EGameMode.pvc:
                    if (playerSide == computerPlayer.side)
                    {
                        computerPlayer.isPcTurn = true;
                        StartCoroutine(ComputerMoves());                        
                    }
                    else
                    {
                        computerPlayer.isPcTurn = false;
                    }
                    break;
                default:
                    break;
            }
        }
        
    }
    void GameOver(string winningPlayer)
    {
        SetBoardInteractable(false);
        if (winningPlayer == "draw")
        {
            SetGameOverText("It's a Draw!");
            SetPlayerColorsInactive();
        }
        else
        {
            SetGameOverText(winningPlayer + " Wins!");
        }
        restartButton.SetActive(true);
    }

}
