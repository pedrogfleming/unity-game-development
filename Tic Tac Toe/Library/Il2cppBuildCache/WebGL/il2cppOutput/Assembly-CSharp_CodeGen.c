﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ComputerPlayer::.ctor()
extern void ComputerPlayer__ctor_m43FFA8B221965B9E2051A126AE60DA82493586BB (void);
// 0x00000002 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000003 System.Void PlayerColor::.ctor()
extern void PlayerColor__ctor_mA0596A90F001F580D4F87FD6B1F3FC399F714685 (void);
// 0x00000004 System.Void GameController::Awake()
extern void GameController_Awake_mE678A4EEF012294E485A814D37D93231A90B4BEC (void);
// 0x00000005 System.Void GameController::Update()
extern void GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0 (void);
// 0x00000006 System.Void GameController::ComputerGetLegalMoves()
extern void GameController_ComputerGetLegalMoves_m502B4036D2CA5B34B5392EA27F1942ECBFBD83F7 (void);
// 0x00000007 System.Void GameController::CalculateRiskingMovements()
extern void GameController_CalculateRiskingMovements_m26FD8FB03FF31D4DEF576B398F04538B581A20AA (void);
// 0x00000008 System.Void GameController::SetGameMode(System.Int32)
extern void GameController_SetGameMode_m0EEFE75475BC37E5E9FCBC66A97BEA13C9D0B29F (void);
// 0x00000009 System.Collections.IEnumerator GameController::ComputerMoves()
extern void GameController_ComputerMoves_m5A59F9A8BC1EA45AAB6A2A2413AFDD5DAFF278A3 (void);
// 0x0000000A System.Void GameController::SetGameControllerReferenceOnButtons()
extern void GameController_SetGameControllerReferenceOnButtons_mF3B73B9E0E62A360FDF97EFF6BA27F18FCCC53FE (void);
// 0x0000000B System.String GameController::GetPlayerSide()
extern void GameController_GetPlayerSide_mE2D7181C037B2400A151B89A3FE07C4DA4E26123 (void);
// 0x0000000C System.Void GameController::SetPlayerColors(Player,Player)
extern void GameController_SetPlayerColors_mADCB18D41FF8D9094159EA1502D075E29706906D (void);
// 0x0000000D System.Void GameController::ChangeSides()
extern void GameController_ChangeSides_mDBAC5E7706D64D3E86154027FE3259D3D9613707 (void);
// 0x0000000E System.Void GameController::SetGameOverText(System.String)
extern void GameController_SetGameOverText_m0542FAF6A3BFCB11CF60C0310AC9BBDE2B66F60B (void);
// 0x0000000F System.Void GameController::RestartGame()
extern void GameController_RestartGame_mE41DCE782C5FB2DFAC6A34D299DD4E06F8490CF3 (void);
// 0x00000010 System.Void GameController::StartGame()
extern void GameController_StartGame_m0FFBAB8E46D3009DE6851DF384B62AEBF328922D (void);
// 0x00000011 System.Void GameController::SetPlayerButtons(System.Boolean)
extern void GameController_SetPlayerButtons_m85354F35EA47E2E1538812BE6291B82BBF2CD24D (void);
// 0x00000012 System.Void GameController::SetPlayerColorsInactive()
extern void GameController_SetPlayerColorsInactive_mAEC5CE38C93B89EE82BDF6103888531DC86F48BA (void);
// 0x00000013 System.Void GameController::SetStartingSide(System.String)
extern void GameController_SetStartingSide_m9D283B6D0DBB032F0AF4EBB1C57AD18406978FDA (void);
// 0x00000014 System.Void GameController::SetBoardInteractable(System.Boolean)
extern void GameController_SetBoardInteractable_mE581503FBDB5C726544036E01B056E77582C0C2E (void);
// 0x00000015 System.Void GameController::EndTurn()
extern void GameController_EndTurn_mAC3BCBCF42E9D2F2738E68259262424450C289CC (void);
// 0x00000016 System.Void GameController::GameOver(System.String)
extern void GameController_GameOver_m89339896EE1F9877C5FFC12C822D1B1A6955FD2E (void);
// 0x00000017 System.Void GameController::.ctor()
extern void GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643 (void);
// 0x00000018 System.Void GameController/<ComputerMoves>d__22::.ctor(System.Int32)
extern void U3CComputerMovesU3Ed__22__ctor_m6B3036984A714305EE8931C2D165C133FDAA0311 (void);
// 0x00000019 System.Void GameController/<ComputerMoves>d__22::System.IDisposable.Dispose()
extern void U3CComputerMovesU3Ed__22_System_IDisposable_Dispose_m29C503C24975FDD6112E58F7756E972BED16705F (void);
// 0x0000001A System.Boolean GameController/<ComputerMoves>d__22::MoveNext()
extern void U3CComputerMovesU3Ed__22_MoveNext_m22CDED58320FF90B0080D75F9EAF72FD01584718 (void);
// 0x0000001B System.Object GameController/<ComputerMoves>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CComputerMovesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m941AF36872FB8573A1D22D9276D2E3521A67463D (void);
// 0x0000001C System.Void GameController/<ComputerMoves>d__22::System.Collections.IEnumerator.Reset()
extern void U3CComputerMovesU3Ed__22_System_Collections_IEnumerator_Reset_m23BC1623F45BC98A2EDEE8551384D8B88EB8287F (void);
// 0x0000001D System.Object GameController/<ComputerMoves>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CComputerMovesU3Ed__22_System_Collections_IEnumerator_get_Current_m5B99D9869A145A2E268435A0E37B0C55A0F11C2F (void);
// 0x0000001E System.Void GridSpace::SetGameControllerReference(GameController)
extern void GridSpace_SetGameControllerReference_mD4F3B236E3955E1239C016B745DB3F1772D7B97E (void);
// 0x0000001F System.Void GridSpace::SetSpace()
extern void GridSpace_SetSpace_mF9A073F16C60B548258E08731D621913B860C7B2 (void);
// 0x00000020 System.Void GridSpace::.ctor()
extern void GridSpace__ctor_m304AA86CD511C2F067F7742F165699B9ABCBCE81 (void);
static Il2CppMethodPointer s_methodPointers[32] = 
{
	ComputerPlayer__ctor_m43FFA8B221965B9E2051A126AE60DA82493586BB,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	PlayerColor__ctor_mA0596A90F001F580D4F87FD6B1F3FC399F714685,
	GameController_Awake_mE678A4EEF012294E485A814D37D93231A90B4BEC,
	GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0,
	GameController_ComputerGetLegalMoves_m502B4036D2CA5B34B5392EA27F1942ECBFBD83F7,
	GameController_CalculateRiskingMovements_m26FD8FB03FF31D4DEF576B398F04538B581A20AA,
	GameController_SetGameMode_m0EEFE75475BC37E5E9FCBC66A97BEA13C9D0B29F,
	GameController_ComputerMoves_m5A59F9A8BC1EA45AAB6A2A2413AFDD5DAFF278A3,
	GameController_SetGameControllerReferenceOnButtons_mF3B73B9E0E62A360FDF97EFF6BA27F18FCCC53FE,
	GameController_GetPlayerSide_mE2D7181C037B2400A151B89A3FE07C4DA4E26123,
	GameController_SetPlayerColors_mADCB18D41FF8D9094159EA1502D075E29706906D,
	GameController_ChangeSides_mDBAC5E7706D64D3E86154027FE3259D3D9613707,
	GameController_SetGameOverText_m0542FAF6A3BFCB11CF60C0310AC9BBDE2B66F60B,
	GameController_RestartGame_mE41DCE782C5FB2DFAC6A34D299DD4E06F8490CF3,
	GameController_StartGame_m0FFBAB8E46D3009DE6851DF384B62AEBF328922D,
	GameController_SetPlayerButtons_m85354F35EA47E2E1538812BE6291B82BBF2CD24D,
	GameController_SetPlayerColorsInactive_mAEC5CE38C93B89EE82BDF6103888531DC86F48BA,
	GameController_SetStartingSide_m9D283B6D0DBB032F0AF4EBB1C57AD18406978FDA,
	GameController_SetBoardInteractable_mE581503FBDB5C726544036E01B056E77582C0C2E,
	GameController_EndTurn_mAC3BCBCF42E9D2F2738E68259262424450C289CC,
	GameController_GameOver_m89339896EE1F9877C5FFC12C822D1B1A6955FD2E,
	GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643,
	U3CComputerMovesU3Ed__22__ctor_m6B3036984A714305EE8931C2D165C133FDAA0311,
	U3CComputerMovesU3Ed__22_System_IDisposable_Dispose_m29C503C24975FDD6112E58F7756E972BED16705F,
	U3CComputerMovesU3Ed__22_MoveNext_m22CDED58320FF90B0080D75F9EAF72FD01584718,
	U3CComputerMovesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m941AF36872FB8573A1D22D9276D2E3521A67463D,
	U3CComputerMovesU3Ed__22_System_Collections_IEnumerator_Reset_m23BC1623F45BC98A2EDEE8551384D8B88EB8287F,
	U3CComputerMovesU3Ed__22_System_Collections_IEnumerator_get_Current_m5B99D9869A145A2E268435A0E37B0C55A0F11C2F,
	GridSpace_SetGameControllerReference_mD4F3B236E3955E1239C016B745DB3F1772D7B97E,
	GridSpace_SetSpace_mF9A073F16C60B548258E08731D621913B860C7B2,
	GridSpace__ctor_m304AA86CD511C2F067F7742F165699B9ABCBCE81,
};
static const int32_t s_InvokerIndices[32] = 
{
	1109,
	1109,
	1109,
	1109,
	1109,
	1109,
	1109,
	939,
	1078,
	1109,
	1078,
	616,
	1109,
	948,
	1109,
	1109,
	964,
	1109,
	948,
	964,
	1109,
	948,
	1109,
	939,
	1109,
	1097,
	1078,
	1109,
	1078,
	948,
	1109,
	1109,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	32,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
