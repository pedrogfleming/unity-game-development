using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLeft : MonoBehaviour
{
    //public float speed = 30.0f;
    private PlayerController playerControllerScript;
    private GameController gameControllerScript;
    private float leftBound = -15;
    //public bool startingGame;
    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        gameControllerScript = GameObject.Find("GameController").GetComponent <GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!gameControllerScript.startingGame)
        {
            if (playerControllerScript != null && !playerControllerScript.gameOver)
            {
                transform.Translate(Vector3.left * Time.deltaTime * gameControllerScript.speed);
            }        
            if(transform.position.x < leftBound && gameObject.CompareTag("Obstacle"))
            {
                Destroy(gameObject);
            }
        }
    }
}
