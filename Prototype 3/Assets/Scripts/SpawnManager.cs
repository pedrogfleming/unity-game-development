using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject obstaclePrefab;
    private Vector3 spawnPos = new Vector3(25, 0, 0);
    private float starDelay = 2;
    private float repeatRate = 2;
    private PlayerController playerControllerScript;
    private GameController gameControllerScript;
    public GameObject[] prefabs;
    //private MoveLeft moveLeftScript;
    // Start is called before the first frame update
    void Start()
    {
        playerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
        //moveLeftScript = GameObject.Find("Background").GetComponent<MoveLeft>();
        InvokeRepeating("SpawnObstacle", starDelay, repeatRate);
        gameControllerScript = GameObject.Find("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void SpawnObstacle()
    {
        if(!playerControllerScript.gameOver && !gameControllerScript.startingGame)
        {
            int indexPrefab = Random.Range(0, prefabs.Length);
            
            if (indexPrefab == 2)
            {
                int randomCrates = Random.Range(1, 3);
                if (randomCrates == 2)
                {                    
                    Instantiate(prefabs[indexPrefab], spawnPos, obstaclePrefab.transform.rotation);
                    spawnPos += new Vector3(0, 2, 0);
                }
            }
            Instantiate(prefabs[indexPrefab], spawnPos, obstaclePrefab.transform.rotation);
            spawnPos = new Vector3(25, 0, 0);
        }
    }
}
