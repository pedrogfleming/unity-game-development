using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    private float jumpForce = 550;
    private float gravityModifier = 1;
    private bool isOnGround = true;
    private bool isJumping;
    public bool isDashing;
    public bool canDash = true;
    //public float coolDownDashing;
    public float timeCoolDownDashing = 3.0f;
    
    public bool gameOver;
    private float upperBound = 5.0f;

    private Animator playerAnim;
    public ParticleSystem explosionParticle;
    public ParticleSystem dirtParticle;
    public AudioClip jumpSound;
    public AudioClip crashSound;
    private AudioSource playerAudio;

    //private MoveLeft scriptMoveLeft;
    private GameController gameControllerScript;
    private float initialVelocityGameplay;
    public float holdingTimeKey = 0.0f;
    private float gravityScale = 5;

    public float playerScore = 0.0f;
    private int multiplierScore = 1;

    private Vector3 playerPosition;
    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        Physics.gravity *= gravityModifier;
        playerAnim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        gameControllerScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        initialVelocityGameplay = gameControllerScript.speed;
        dirtParticle.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        #region Startin Animation  Walk Entry
        playerPosition = playerRb.position;
        if(gameControllerScript.startingGame)
        {
            if (playerPosition.x < 0)
            {
                transform.Translate(Vector3.forward * Time.deltaTime);
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().pitch = -1;
            }
            else /*if (playerPosition.x >= 0)*/
            {
                dirtParticle.Play();
                playerAnim.SetBool("Start_Running_b", true);
                gameControllerScript.startingGame = false;
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().pitch = 1;
            }
        }
        #endregion
        if (!gameOver && !gameControllerScript.startingGame)
        {
            
            playerScore += Time.deltaTime * multiplierScore;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (isOnGround)
                {
                    JumpPlayer();
                    isJumping = true;
                    isOnGround = false;
                }
                else if (isJumping)
                {
                    JumpPlayer();
                    isJumping = false;
                }
            }
            if(canDash)
            {
                if(!isDashing && Input.GetKey(KeyCode.Q))
                {
                    StartDashing();
                    gameControllerScript.speed *= 2;
                    multiplierScore = 2;
                }
                else if(Input.GetKey(KeyCode.Q))
                {
                    holdingTimeKey += Time.deltaTime;
                }
            }
            else
            {
                timeCoolDownDashing -= Time.deltaTime;
            }
            
            if (timeCoolDownDashing <= 0)
            {
                timeCoolDownDashing = 3.0f;
                canDash = true;
            }
            if(Input.GetKeyUp(KeyCode.Q))
            {
                StopDashing();
                gameControllerScript.speed = initialVelocityGameplay;                 
            }
            if(holdingTimeKey > 3)
            {
                StopDashing();
                gameControllerScript.speed = initialVelocityGameplay;
            }
        }
        if (transform.position.y > upperBound)
        {
            PullDownPlayer();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        
        if (!gameOver && !gameControllerScript.startingGame && collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
            dirtParticle.Play();
        }
        else if(collision.gameObject.CompareTag("Obstacle"))
        {
            gameOver = true;
            Debug.Log("Game Over!");
            Debug.Log("Final Score:");
            Debug.Log(playerScore);
            playerAnim.SetBool("Death_b", true);
            playerAnim.SetInteger("DeathType_int", 1);
            explosionParticle.Play();
            dirtParticle.Stop();
            playerAudio.PlayOneShot(crashSound, 1.0f);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().pitch = 0.2f;
        }
    }
    private void JumpPlayer()
    {
        playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);        
        playerAnim.SetTrigger("Jump_trig");
        dirtParticle.Stop();
        playerAudio.PlayOneShot(jumpSound, 1.0f);

    }
    private void StopDashing()
    {
        //if(scriptMoveLeft.speed > initialVelocityGameplay)
        //{
        //    scriptMoveLeft.speed /= 2;
        //}
        //gameControllerScript.speed -= 2;
        multiplierScore = 1;
        holdingTimeKey = 0;
        isDashing = false;
        canDash = false;
        //Dessacelarate the music
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().pitch = 1;
    }
    private void StartDashing()
    {
        
        //gameControllerScript.speed += initialVelocityGameplay;
        holdingTimeKey += Time.deltaTime;
        isDashing = true;
        //Acelerate the music
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().pitch = 2;
    }
    private void PullDownPlayer()
    {
        transform.position = new Vector3(transform.position.x, upperBound, transform.position.z);
        //Simulate gravity scale to make the player go down faster, changing the player�s gravity only
        playerRb.AddForce(Physics.gravity * (gravityScale - 1) * playerRb.mass);
    }
}
