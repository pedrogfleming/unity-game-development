﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GameController::Start()
extern void GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE (void);
// 0x00000002 System.Void GameController::Update()
extern void GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0 (void);
// 0x00000003 System.Void GameController::.ctor()
extern void GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643 (void);
// 0x00000004 System.Void MoveLeft::Start()
extern void MoveLeft_Start_m87B4DA26D582023B9115A7EDEBBEB081FFCE6D0B (void);
// 0x00000005 System.Void MoveLeft::Update()
extern void MoveLeft_Update_mBD382817D0425A25BC5BACE7CF95B39CCC2714E6 (void);
// 0x00000006 System.Void MoveLeft::.ctor()
extern void MoveLeft__ctor_mE380DDBD24DB8DCDAB59AFCD04F9BD9B041560CC (void);
// 0x00000007 System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x00000008 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000009 System.Void PlayerController::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerController_OnCollisionEnter_m68BA6D0E1F4613D0A8E7A86BA53F2A94AF4D9C84 (void);
// 0x0000000A System.Void PlayerController::JumpPlayer()
extern void PlayerController_JumpPlayer_mF98D9948B281E7884352A7EE5BFEEE891D31C88D (void);
// 0x0000000B System.Void PlayerController::StopDashing()
extern void PlayerController_StopDashing_mA9B3BC12EEDE725DFB76DDE8FE2FE6098B9D79A0 (void);
// 0x0000000C System.Void PlayerController::StartDashing()
extern void PlayerController_StartDashing_mE2EA61CF3ED8490C441239F7A45AD34CF47CC65E (void);
// 0x0000000D System.Void PlayerController::PullDownPlayer()
extern void PlayerController_PullDownPlayer_mBED450727CBF0921C5A21E7E12920BE2403BC023 (void);
// 0x0000000E System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x0000000F System.Void RepeatBackground::Start()
extern void RepeatBackground_Start_m78F2061AD036CDAE128CEBF1197907F71A169E68 (void);
// 0x00000010 System.Void RepeatBackground::Update()
extern void RepeatBackground_Update_m236DB8CEE5FE8C096D113FF3ADE8C3CD3A2CB0FF (void);
// 0x00000011 System.Void RepeatBackground::.ctor()
extern void RepeatBackground__ctor_mD8350484937A4901554C93AAFE45968E75B65AA6 (void);
// 0x00000012 System.Void SpawnManager::Start()
extern void SpawnManager_Start_mFA050B5C59C7CC47F4430CA0F5A646051D5442CB (void);
// 0x00000013 System.Void SpawnManager::Update()
extern void SpawnManager_Update_m68C1739F474F0D29A0BF533BD44BA82786A3C96B (void);
// 0x00000014 System.Void SpawnManager::SpawnObstacle()
extern void SpawnManager_SpawnObstacle_mBF58F770C09F759699E18E26AD674761CB389299 (void);
// 0x00000015 System.Void SpawnManager::.ctor()
extern void SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59 (void);
static Il2CppMethodPointer s_methodPointers[21] = 
{
	GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE,
	GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0,
	GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643,
	MoveLeft_Start_m87B4DA26D582023B9115A7EDEBBEB081FFCE6D0B,
	MoveLeft_Update_mBD382817D0425A25BC5BACE7CF95B39CCC2714E6,
	MoveLeft__ctor_mE380DDBD24DB8DCDAB59AFCD04F9BD9B041560CC,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_OnCollisionEnter_m68BA6D0E1F4613D0A8E7A86BA53F2A94AF4D9C84,
	PlayerController_JumpPlayer_mF98D9948B281E7884352A7EE5BFEEE891D31C88D,
	PlayerController_StopDashing_mA9B3BC12EEDE725DFB76DDE8FE2FE6098B9D79A0,
	PlayerController_StartDashing_mE2EA61CF3ED8490C441239F7A45AD34CF47CC65E,
	PlayerController_PullDownPlayer_mBED450727CBF0921C5A21E7E12920BE2403BC023,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	RepeatBackground_Start_m78F2061AD036CDAE128CEBF1197907F71A169E68,
	RepeatBackground_Update_m236DB8CEE5FE8C096D113FF3ADE8C3CD3A2CB0FF,
	RepeatBackground__ctor_mD8350484937A4901554C93AAFE45968E75B65AA6,
	SpawnManager_Start_mFA050B5C59C7CC47F4430CA0F5A646051D5442CB,
	SpawnManager_Update_m68C1739F474F0D29A0BF533BD44BA82786A3C96B,
	SpawnManager_SpawnObstacle_mBF58F770C09F759699E18E26AD674761CB389299,
	SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59,
};
static const int32_t s_InvokerIndices[21] = 
{
	863,
	863,
	863,
	863,
	863,
	863,
	863,
	863,
	748,
	863,
	863,
	863,
	863,
	863,
	863,
	863,
	863,
	863,
	863,
	863,
	863,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	21,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
