﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public MeshRenderer Renderer;
    float xAngleRotation = 0;
    float yAngleRotation = 0;
    float zAngleRotation = 0;
    float rotationSpeed = 10.0f;
    Material material;
    void Start()
    { 
        material = Renderer.material;
        int optionCube = Random.Range(0, 6);
        switch (optionCube)
        {
            //Change the cube's location
            case 0:
                transform.position = new Vector3(10, 5, 1);
                break;
            //Change the cube's scale.
            case 1:
                transform.localScale = Vector3.one * 5.0f;
                break;
            //Change the angle at which the cube rotates.
            case 2:
                RandomRotationCube();
                break;
            //Change the cube’s rotation speed.
            case 3:
                rotationSpeed = Random.Range(1, rotationSpeed);
                RandomRotationCube();
                break;
            //Change the cube’s material color.
            case 4:
                RandomColorMaterial();
                break;
            //Change the cube’s material opacity.
            case 5:
                material.color = Color.clear;
                break;
            default:
                break;
        }
        Debug.Log(optionCube);
    }
    
    void Update()
    {
        //transform.Rotate(rotationSpeed * Time.deltaTime, yAngleRotation, zAngleRotation);
        transform.Rotate(xAngleRotation * Time.deltaTime, yAngleRotation * Time.deltaTime, zAngleRotation * Time.deltaTime);
    }
    private void RandomRotationCube()
    {
        int randomRotation = Random.Range(0, 3);
        switch (randomRotation)
        {
            case 0:
                xAngleRotation = 5.0f;
                yAngleRotation = 0;
                zAngleRotation = 0;
                break;
            case 1:
                xAngleRotation = 0;
                yAngleRotation = 5.0f;
                zAngleRotation = 0;
                break;
            case 2:
                xAngleRotation = 0;
                yAngleRotation = 0;
                zAngleRotation = 5.0f;
                break;
        }
    }
    private void RandomColorMaterial()
    {
        int randomColor = Random.Range(0, 3);
        float r = 0.0f;
        float g = 0.0f;
        float b = 0.0f;
        switch (randomColor)
        {
            case 0:
                r = Random.Range(0.0f, 1);
                break;
            case 1:
                g = Random.Range(0.0f, 1);
                break;
            case 2:
                b = Random.Range(0.0f, 1);
                break;
        }
        material.color = new Color(r, g, b);
    }

}
